import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/sections/Index'
import Cart from '@/sections/Cart'
import MenApparel from '@/sections/MenApparel'
import WomenApparel from '@/sections/WomenApparel'
import Supplements from '@/sections/Supplements'
import Accessories from '@/sections/Accessories'
import ProductPage from '@/sections/ProductPage'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index
    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart
    },
    {
      path: '/menApparel',
      name: 'menApparel',
      component: MenApparel
    },
    {
      path: '/womenApparel',
      name: 'womenApparel',
      component: WomenApparel
    }
    ,
    {
      path: '/accessories',
      name: 'accessories',
      component: Accessories
    },
    {
      path: '/supplements',
      name: 'supplements',
      component: Supplements
    },
    {
      path: '/productpage',
      name: 'productpage',
      component: ProductPage
    }
  ]
})
