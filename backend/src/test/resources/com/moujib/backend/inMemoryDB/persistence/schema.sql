CREATE TABLE address (
  id DECIMAL NOT NULL,
  box VARCHAR(255),
  city VARCHAR(255),
  number DECIMAL NOT NULL,
  street_name VARCHAR(255),
  zip_code DECIMAL NOT NULL
);


CREATE TABLE basket (
  id DECIMAL NOT NULL,
  created_at TIMESTAMP,
  total_price real NOT NULL,
  customer_id DECIMAL
);



CREATE TABLE basket_row (
  id DECIMAL NOT NULL,
  quantity DECIMAL NOT NULL,
  total_price real NOT NULL,
  basket_id DECIMAL,
  product_id DECIMAL
);


CREATE TABLE clothe (
  id DECIMAL NOT NULL,
  created_at TIMESTAMP,
  description VARCHAR(255),
  in_stock boolean NOT NULL,
  name VARCHAR(255),
  price real NOT NULL,
  clothe_type VARCHAR(255),
  sex VARCHAR(255),
  size VARCHAR(255)
);


CREATE TABLE customer (
  id DECIMAL NOT NULL,
  birth_date TIMESTAMP,
  created_at TIMESTAMP,
  email VARCHAR(255),
  last_name VARCHAR(255),
  name VARCHAR(255),
  password VARCHAR(255),
  username VARCHAR(255),
  address_id DECIMAL,
  basket_id DECIMAL
);

CREATE TABLE equipment (
  id DECIMAL NOT NULL,
  created_at TIMESTAMP,
  description VARCHAR(255),
  in_stock boolean NOT NULL,
  name VARCHAR(255),
  price real NOT NULL,
  equipment_type VARCHAR(255)
);


CREATE TABLE food (
  id DECIMAL NOT NULL,
  created_at TIMESTAMP,
  description VARCHAR(255),
  in_stock boolean NOT NULL,
  name VARCHAR(255),
  price real NOT NULL,
  food_type VARCHAR(255)
);


CREATE TABLE rating (
  id DECIMAL NOT NULL,
  comment VARCHAR(255),
  product_id DECIMAL,
  user_id DECIMAL
);

