INSERT INTO customer (birth_date, created_at, email, last_name, name, password, username, address_id, basket_id) VALUES ('30-09-92 00:00:00.0', '25-05-18 00:00:00.0', 'moujibsoulaiman@gmail.com', 'Moujib', 'Solaiman', 'mdp1234', 'Sareys', 1, 1);
INSERT INTO customer (birth_date, created_at, email, last_name, name, password, username, address_id, basket_id) VALUES ('07-03-86 00:00:00.0', '25-05-18 00:00:00.0', 'utilisateur@gmail.com', 'Incognito', 'Marcelo', 'motdepasse', 'Incognito', 2, 2);

INSERT INTO address (box, city, number, street_name, zip_code) VALUES ('5C', 'Bruxelles', '86', 'Rue de Bosnie', 1060);
INSERT INTO address (box, city, number, street_name, zip_code) VALUES ('3', 'Charleroi', '86', 'Avenue Fosny', 1060);
INSERT INTO address (box, city, number, street_name, zip_code) VALUES ('1', 'Bruxelles', '86', 'Rue de Ailes', 1030);

INSERT INTO basket (created_at, total_price, customer_id) VALUES ('23-01-18 00:00:00.0', 1);
INSERT INTO basket (created_at, total_price, customer_id) VALUES ('19-09-17 00:00:00.0', 2);
INSERT INTO basket (created_at, total_price, customer_id) VALUES ('22-02-18 00:00:00.0', 3);

INSERT INTO basket_row (quantity, total_price, basket_id, product_id) VALUES (2, 100, 1, 1);
INSERT INTO basket_row (quantity, total_price, basket_id, product_id) VALUES (1, 150, 1, 2);
INSERT INTO basket_row (quantity, total_price, basket_id, product_id) VALUES (2, 260, 2, 3);
INSERT INTO basket_row (quantity, total_price, basket_id, product_id) VALUES (6, 600, 3, 4);

INSERT INTO clothe (created_at, description, in_stock, name, price, clothe_type, sex, size) VALUES ('23-01-18 00:00:00.0', 'A Nice Jacket', true, 'Nike Jacket', 50, 'upper', 'male', 'medium');
INSERT INTO clothe (created_at, description, in_stock, name, price, clothe_type, sex, size) VALUES ('30-12-05 00:00:00.0', 'A Nice Pants', false, 'Adidas Jacket', 30, 'lower', 'male', 'medium');
INSERT INTO clothe (created_at, description, in_stock, name, price, clothe_type, sex, size) VALUES ('14-03-12 00:00:00.0', 'A Nice Jacket', true, 'Nike Jacket', 20, 'upper', 'female', 'medium');
INSERT INTO clothe (created_at, description, in_stock, name, price, clothe_type, sex, size) VALUES ('20-07-17 00:00:00.0', 'A Nice Pants', false, 'Adidas Jacket', 60, 'lower', 'female', 'medium');

INSERT INTO equipment (created_at, description, in_stock, name, price, equipment_type) VALUES ('23-01-18 00:00:00.0', 'A leather jump rope', true, 'Leather Rope', 15, null);
INSERT INTO equipment (created_at, description, in_stock, name, price, equipment_type) VALUES ('05-11-18 00:00:00.0', 'A Heavy KettleBell', true, 'Kettle Bell', 50, null);
INSERT INTO equipment (created_at, description, in_stock, name, price, equipment_type) VALUES ('12-01-18 00:00:00.0', 'A lifting belt', true, 'Lifting Belt', 40, null);

INSERT INTO food (created_at, description, in_stock, name, price, food_type) VALUES ('23-01-18 00:00:00.0', 'Strawberry Isolate Whey', true, 'Qjbsfjdbsf sdfksdbfjsdbf', 15, 'whey');
INSERT INTO food (created_at, description, in_stock, name, price, food_type) VALUES ('12-02-18 00:00:00.0', 'Chocolate Multi Vitamin', true, 'Qkjbhdfkjbfsdjk dfkjsdbfjksdb', 46, 'vitamins');

INSERT INTO rating (comment, product_id, user_id) VALUES ('Very very very very good', 1, 1);
INSERT INTO rating (comment, product_id, user_id) VALUES ('Very good equipment', 2, 2);
INSERT INTO rating (comment, product_id, user_id) VALUES ('Meh', 3, 1);
INSERT INTO rating (comment, product_id, user_id) VALUES ('Very Shit', 4, 2);
