package com.moujib.backend.controller.product;

import com.moujib.backend.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//Use this class to return new Products or recommendation on the front page
//TODO: Change all @Controller with @RestController(Eliminates the need to add @ResponseBody to each method)
@RestController
@RequestMapping("/product")
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController (ProductService productService){
        this.productService = productService;
    }

}